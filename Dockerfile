FROM registry.cern.ch/cloud/cci-openstack-client:cern

COPY config9al-stable.repo linuxsupport9al-stable.repo /etc/yum.repos.d/
COPY ai.conf /etc/ai/

RUN dnf install -y epel-release
RUN dnf install -y \
    ai-tools \
    libguestfs-tools-c \
    openssh-clients \
    jq \
    nc \
  && dnf clean all

ENV OS_PROJECT_NAME="IT Linux Support - CI VMs"
ENV SHELL=bash

CMD /bin/bash
